package main

import (
	"encoding/binary"
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3/driver"
)

// CommandDumper is a hook to the driver that dumps all the command the driver
// executes.
type CommandDumper struct {
}

// Type specifies the type it hooks to.
func (c *CommandDumper) Type() reflect.Type {
	return reflect.TypeOf((driver.Command)(nil))
}

// Pos specifies the position it hooks to.
func (c *CommandDumper) Pos() akita.HookPos {
	return akita.AnyHookPos
}

// Func calculates the time spent on kernel execution.
func (c *CommandDumper) Func(
	item interface{},
	domain akita.Hookable,
	info interface{},
) {
	cmd, ok := item.(driver.Command)
	if !ok {
		return
	}
	hookInfo := info.(*driver.CommandHookInfo)

	if hookInfo.IsStart {
		log.Printf("%.15f: Command %s starts on GPU %d\n",
			hookInfo.Now, reflect.TypeOf(cmd),
			hookInfo.Queue.GPUID)
		c.moreCmdInfo(cmd)
	} else {
		log.Printf("%.15f: Command %s ends on GPU %d\n",
			hookInfo.Now, reflect.TypeOf(cmd),
			hookInfo.Queue.GPUID)
	}
}

func (c *CommandDumper) moreCmdInfo(cmd driver.Command) {
	switch cmd := cmd.(type) {
	case *driver.MemCopyH2DCommand:
		log.Printf("\t length: %d", binary.Size(cmd.Src))
	case *driver.MemCopyD2HCommand:
		log.Printf("\t length: %d", binary.Size(cmd.Dst))
	case *driver.LaunchKernelCommand:
		log.Printf("\t code object: %#v, packet: %#v", cmd.CodeObject.HsaCoHeader, cmd.Packet)
	default:
		log.Printf("\t detail not available")
	}
}
